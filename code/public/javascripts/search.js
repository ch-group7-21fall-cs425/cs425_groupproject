function CustomDropDown(element) {
  this.dropdown = element;
  this.placeholder = this.dropdown.find(".placeholder");
  this.options = this.dropdown.find("ul.dropdown-menu > li");
  this.val = '0';
  this.index = -1;
  this.initEvents();
}

CustomDropDown.prototype = {
  initEvents: function() {
    var obj = this;

    obj.options.on("click", function() {
      var opt = $(this);
      obj.text = opt.find("a").text();
      obj.val = opt.attr("value");
      obj.index = opt.index();
      obj.placeholder.text(obj.text);
    });
  },
  getText: function() {
    return this.text;
  },
  getValue: function() {
    return this.val;
  },
  getIndex: function() {
    return this.index;
  }
};

function RadioSelect(element) {
  this.obj = element;

  this.options = this.obj.find('input');

  this.val = 'all';
  this.initEvents();
}

RadioSelect.prototype = {
  initEvents: function() {
    var obj = this;
    obj.options.on('change', function() {
      var opt = $(this);
      obj.val = opt.attr("value");
    });
  },
  getValue: function() {
    return this.val;
  }
};

$(document).ready(function() {
  var mydropdown = new CustomDropDown($("#dropdown1"));
  var myradio = new RadioSelect($("#radioselect"));

  $('#submit').click(function() {
    var url = '/search/r';
    var searchType = mydropdown.getValue();
    var bookType = myradio.getValue();
    var content = $('#content').val();
    if (content == '') {
      alert('please input something');
    } else {
      url = url + "?sType=" + searchType + "&bType=" + bookType + "&content=" + content;
      document.location.href = url;
    }
  });
});

$(document).ready(function() {
  $('#addbook').click(function() {
    document.location.href = '/mylib/addbook';
  });
});