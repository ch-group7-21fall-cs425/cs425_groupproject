var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var Book = require('../models/book');
var Borrow = require('../models/borrow');

router.get('/', function (req, res) {
	res.redirect('/search');
});

router.get('/search', function (req, res) {
	res.render('search', {
		arr: [{ sch: 'active', lib: '', abt: '', log: '' }]
	});
});

router.get('/search/r', function (req, res, next) {
	var searchType = req.query.sType;
	var bookType = req.query.bType;
	var content = req.query.content;
	console.log('content', content);
	Book.findBooksByTitle(content,bookType, function (err, books) {
		if (err) {
			return next(err);
		}
		books.forEach(function (book) {
			if (book.type == 0) {
				book.type = 'Books';
			}

			if (book.type == 1) {
				book.type = 'Journal Articles';
			}

			if (book.type == 3) {
				book.type = 'Magazines';
			}
		});
		res.render('result', {
			arr: [{ sch: 'active', lib: '', abt: '', log: '' }],
			books: books,
		});
	});
});

router.get('/books', function (req, res, next) {
	var isbn = req.query['isbn'];
	Book.findBooksByISBN(isbn, function (err, bookInfo, booksState) {
		if (err) {
			return next(err);
		}

		booksState.forEach(function (book) {
			if (book.state == 0) {
				book.state = 'Loanable';
				book.isBorrow = '<a href="/borrow?barcode=' + book.barcode + '">Borrow</a>';
			} else if (book.state == 1) {
				book.state = 'loan';
				book.isBorrow = 'The book has been lent out';
			}
		});

		res.render('books', {
			arr: [{ sch: 'active', lib: '', abt: '', log: '' }],
			bookInfo: bookInfo,
			booksState: booksState,
		});
	});
});

router.get('/borrow', ensureAuthenticated, function (req, res, next) {
	var barcode = req.query['barcode'];
	Book.findBookBybarcode(barcode, function (err, book) {
		if (err) {
			return next(err);
		}

		const inDate = new Date()
		inDate.setMonth(inDate.getMonth() + 1)
		inDate.toLocaleDateString()
		book.inDate = inDate.toLocaleDateString()

		res.render('borrow', {
			arr: [{ sch: 'active', lib: '', abt: '', log: '' }],
			book: book
		});
	});
});

router.post('/borrow', ensureAuthenticated, function (req, res, next) {
	var barcode = req.body.barcode;
	var readerId = res.locals.user.readerId;
	Borrow.save(readerId, barcode, function (err) {
		if (err) {
			return next(err);
		}
		res.render('result_borrow', {
			arr: [{ sch: 'active', lib: '', abt: '', log: '' }]
		});
	});
});

router.get('/login', function (req, res) {
	res.render('login', {
		arr: [{ sch: '', lib: 'active', abt: '', log: '' }]
	});
});

router.post('/login', function (req, res, next) {
	var referer = req.body.referer;
	passport.authenticate('local', function (err, user, info) {
		if (err) {
			return next(err);
		}
		if (!user) {
			req.flash('error_msg', info.message);
			return res.redirect('/login');
		}
		req.logIn(user, function (err) {
			if (err) { return next(err); }
			req.flash('success_msg', 'login success...');
			if (referer != 'http://127.0.0.1:3000/login') {
				return res.redirect(referer);
			}
			return res.redirect('/mylib/myborrow');
		});
	})(req, res, next);
});

passport.use(new LocalStrategy(
	function (username, password, done) {
		User.findUserByreaderId(username, function (err, user) {
			if (err) {
				return done(err);
			}
			if (!user) {
				return done(null, false, { message: 'username error!' });
			}
			if (user.password != password) {
				return done(null, false, { message: 'password error!' });
			}
			return done(null, user);
		});
	})
);

passport.serializeUser(function (user, done) {
	done(null, user.readerId);
});

passport.deserializeUser(function (username, done) {
	User.findUserByreaderId(username, function (err, user) {
		done(err, user);
	});
});

router.get('/loginOut', function (req, res) {
	req.logout();
	res.redirect('/login');
});

router.get('/about', function (req, res) {
	res.render('about', {
		arr: [{ sch: '', lib: '', abt: 'active', log: '' }]
	});
});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	} else {
		req.flash('error_msg', 'You are not logged in');
		res.redirect('/login');
	}
}

module.exports = router;