var express = require('express');
var router = express.Router();
var Borrow = require('../models/borrow');
var User = require('../models/user');
var Book = require('../models/book');

router.get('/myborrow', ensureAuthenticated, function (req, res, next) {
	var readerId = res.locals.user.readerId;
	Borrow.findNowBorrow(readerId, function (err, borrows, type) {
		if (err) {
			return next(err);
		}
		borrows.forEach(function (borrow) {
			var outdate = borrow.outdate;
			borrow.outdate = outdate.toLocaleDateString();
			outdate.setMonth(outdate.getMonth() + 1);
			borrow.indate = outdate.toLocaleDateString();

			borrow.overdue = +new Date() > +outdate ? 'overdue' : '';
		});

		if (type === 'admin') {
			res.render('allborrow', {
				arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
				borrows: borrows
			});
		} else {
			res.render('myborrow', {
				arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
				borrows: borrows
			});
		}
	});
});

router.get('/adminuser', ensureAuthenticated, function (req, res, next) {
	var readerId = res.locals.user.readerId;
	User.getUserType(readerId, function (err, info) {
		if (err) {
			return next(err);
		}

		if (info.type === 'student') {
			res.render('no_authority', {
				arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
			});
		} else {
			User.findAllStudent(function (err, users) {
				if (err) {
					return next(err);
				}

				res.render('allusers', {
					arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
					users: users
				});
			})
		}
	})
});

router.get('/adminbook', ensureAuthenticated, function (req, res, next) {
	var readerId = res.locals.user.readerId;
	User.getUserType(readerId, function (err, info) {
		if (err) {
			return next(err);
		}

		if (info.type === 'student') {
			res.render('no_authority', {
				arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
			});
		} else {
			Book.findBook(function (err, books) {
				if (err) {
					return next(err);
				}
				books.forEach(function (book) {
					if (book.type == 0) {
						book.type = 'Books';
					}
		
					if (book.type == 1) {
						book.type = 'Journal Articles';
					}
		
					if (book.type == 3) {
						book.type = 'Magazines';
					}
				});

				res.render('allbook', {
					arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
					books: books
				});
			})
		}
	})
});

router.post('/return', ensureAuthenticated, function (req, res, next) {
	var barcode = req.body.barcode;
	var readerId = res.locals.user.readerId;
	Borrow.returnBook(readerId, barcode, function (err) {
		if (err) {
			return next(err);
		}
		res.render('result_return', {
			arr: [{ sch: '', lib: 'active', abt: '', log: '' }]
		});
	});
});

router.post('/renew', ensureAuthenticated, function (req, res, next) {
	var barcode = req.body.barcode;
	console.log('barcode', barcode);
	var readerId = res.locals.user.readerId;
	Borrow.renew(readerId, barcode, function (err) {
		if (err) {
			return next(err);
		}
		res.render('result_renew', {
			arr: [{ sch: '', lib: 'active', abt: '', log: '' }]
		});
	});
});

router.post('/deluser', ensureAuthenticated, function (req, res, next) {
	var deruserid = req.body.deruserid;

	User.DelUser(deruserid, function (err, result) {
		if (err) {
			return next(err);
		}

		res.render('result_deluser', {
			arr: [{ sch: '', lib: 'active', abt: '', log: '' }]
		});
	})

	return;
});


router.get('/history', ensureAuthenticated, function (req, res, next) {
	var readerId = res.locals.user.readerId;
	Borrow.findHistory(readerId, function (err, rows) {
		if (err) {
			return next(err);
		}
		rows.forEach(function (row) {
			row.indate = row.inDate.toLocaleDateString();
			row.outdate = row.outDate.toLocaleDateString();
		});
		res.render('history', {
			arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
			books: rows
		});
	});
});


router.get('/info', ensureAuthenticated, function (req, res) {
	var userInfo = res.locals.user;
	if (userInfo.sex == 'm') {
		userInfo.sex = 'male';
	} else {
		userInfo.sex = 'female';
	}
	res.render('info', {
		arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
		info: userInfo
	});
});

router.get('/addbook', ensureAuthenticated, function (req, res) {
	res.render('addbook', {
		arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
	});
});

router.post('/addbook', ensureAuthenticated, function (req, res) {
	console.log(req.body);

	res.render('addbooksuccess', {
		arr: [{ sch: '', lib: 'active', abt: '', log: '' }],
	});
});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	} else {
		req.flash('error_msg', 'You are not logged in');
		res.redirect('/login');
	}
}

module.exports = router;