var db=require('./dbhelper');
function Book(){
	this.isbn;
	this.barcode;
	this.title;
	this.author;
	this.type;
	this.press;
	this.price;
	this.content;
	this.catalog;
	this.callNumber;
	this.address;
	this.state;
}

module.exports=Book;

Book.prototype.save = function() {
	var sql="";
};

Book.findBooksByTitle=function(title, bookType,callback){
	var sql;

	if (bookType === 'all') {
		sql=`SELECT isbn,title,author,type,press,callNumber FROM book WHERE title LIKE '%${title}%'`;
	} else {
		sql=`SELECT isbn,title,author,type,press,callNumber FROM book WHERE title LIKE '%${title}%' AND type = ${bookType};`;
	}

	db.exec(sql,'',function(err,rows){
		if(err){
			return callback(err);
		}
		callback(err,rows);
	});
};

Book.findBooksByISBN=function(isbn,callback){
	db.getConnection(function(err,connection){
		if(err){
			return callback(err);
		}
		var sql;
		connection.beginTransaction(function(err){
			if(err){
				return callback(err);
			}
			sql="SELECT isbn,title,author,press,price,content,catalog,address,callNumber FROM book WHERE isbn='"+isbn+"';";
			connection.query(sql,[],function(err,rows){
				if(err){
					return connection.rollback(function(){
						callback(err);
					});
				}
				var bookInfo=rows[0];//图书基本信息

				sql="SELECT bk.callNumber,ib.barcode,bk.address,ib.state FROM book bk,isbn_barcode ib WHERE ib.isbn='"+isbn+"' AND bk.isbn=ib.isbn;";
				connection.query(sql,[],function(err,rows){
					if(err){
						return connection.rollback(function(){
							callback(err);
						});
					}
					var booksState=rows;//图书馆藏情况

					connection.commit(function(err){
						if(err){
							return connection.rollback(function(){
								callback(err);
							});
						}
						connection.end();
						callback(undefined,bookInfo,booksState);
					});
				});
			});
		});
	});
};

Book.findBookBybarcode=function(barcode,callback){
	var sql="SELECT bk.title,ib.barcode,bk.author,bk.press FROM book bk,isbn_barcode ib WHERE barcode='"+barcode+"' AND bk.isbn=ib.isbn;";
	db.exec(sql,'',function(err,rows){
		if(err){
			callback(err);
		}
		callback(err,rows[0]);
	});
};

Book.findBook=function(callback){
	var sql="SELECT * from book";
	db.exec(sql,'',function(err,rows){
		if(err){
			callback(err);
		}
		callback(err,rows);
	});
};

Book.addNewBook=function(callback){
	// Not implemented
	var sql=`INSERT INTO table_name VALUES (value1,value2,value3,...);`;

	// db.exec(sql,'',function(err,rows){
	// 	if(err){
	// 		callback(err);
	// 	}
	// 	callback(err,rows[0]);
	// });

	callback(true);
};