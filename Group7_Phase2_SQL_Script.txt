﻿CREATE DATABASE LMS;

CREATE TABLE Users(
	UserID int Primary key,
	readerId  varchar(10) NOT NULL,
	UserType int,
	Username char(16),
	sex char(1) NOT NULL,
	Password char(200),
	maxBorrow  tinyint DEFAULT NULL,
  maxOrder  tinyint DEFAULT NULL,
  creditNum  varchar(18) DEFAULT NULL,
  job  varchar(20) DEFAULT NULL,
  breakRule  tinyint DEFAULT NULL,  
	LastName varchar(255),
	FirstName varchar(255),
	Phone varchar(255),
	Address varchar(255),
	Email varchar(255),
	UNIQUE KEY  readerId  ( readerId )
);
Create Index index1 on Users(UserID asc)

CREATE TABLE Documents(
	ISBN char(13) Primary key,
	DocType int not null,
	Title varchar(255) not null,
	TotalCopies int not null,
	TotalCopies int not null,
	Catalog tinytext,	
	callNumber varchar(20) NOT NULL,
	address varchar(50) NOT NULL,
	classification varchar(255),
	Publisher varchar(255),
  publish_time date DEFAULT NULL,
	Description varchar(255)
);
Create Index index2 on Documents(DocType asc);
Create Index index3 on Documents(Title  asc);
Create Index index4 on Documents(Publisher  asc);

CREATE TABLE Authors(
	AuthorsID int,
	ISBN int,
	Author varchar(255) not null,
	Description varchar(255)
);
alter table Authors add constraint pk1_name primary key (AuthorsID,DocID);

CREATE TABLE Keywords(
	KeywordsID int,
	ISBN int,	
	Keyword varchar(255) not null
);
alter table Keywords add constraint pk2_name primary key (KeywordsID ,DocID)
);

CREATE TABLE Books(
	BookID int not null,
	DocID int not null,
	Status int not null,
	Edition varchar(255)
);
alter table Books add constraint pk3_name primary key (BookID ,DocID);
Create Index index5 on Books(DocID asc,BookID asc);


CREATE TABLE JournalArticles(
	JAID int not null,
	DocID int not null,
	Status int not null,
	JournalName varchar(255),
	PublicationDate varchar(255)
);
alter table JournalArticles add constraint pk4_name primary key (JAID ,DocID);
Create Index index6 on JournalArticles(JAID  asc,BookID asc);

CREATE TABLE Magazines(
	MagzID int not null,
	DocID int not null,
	Status int not null,
	AuthorsID int not null, 
	ContributorsID int not null    #Authors
);
alter table Magazines add constraint pk5_name primary key (MagzID ,DocID);
Create Index index7 on Magazines(MagzID asc,BookID asc);

CREATE TABLE Thesis(
	ThesisID int Primary key,
	DocID int not null,
	Advisor varchar(255)
);
alter table Thesis add constraint pk6_name primary key (ThesisID ,DocID);

CREATE TABLE TechnicalReports(
	TRID int Primary key,
	DocID int not null,
	Feature varchar(255)
);
alter table TechnicalReports add constraint pk7_name primary key (TRID  ,DocID);

CREATE TABLE Borrow(
	MemberID int,
	ISBN char(13),
  barcode char(8) NOT NULL,
  outDate datetime NOT NULL,
  frequency char(1) DEFAULT NULL,
  PRIMARY KEY ( MemberID , ISBN , barcode )
);
Create Index index8 on Borrow(outDate asc);

CREATE TABLE history (
  MemberID  varchar(10) NOT NULL,
  barcode char(8) NOT NULL,
  outDate datetime NOT NULL,
  inDate datetime NOT NULL,
  PRIMARY KEY ( readerId , barcode , outDate,inDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for isbn_barcode
-- ----------------------------
CREATE TABLE  isbn_barcode  (
   ISBN  char(13) NOT NULL,
   barcode  char(8) NOT NULL,
   state  tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY ( barcode )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS  order ;
CREATE TABLE  order  (
   readerId  varchar(10) NOT NULL,
   isbn  char(13) NOT NULL,
   orderDate  datetime NOT NULL,
   inDate  datetime DEFAULT NULL,
   state  tinyint NOT NULL,
  PRIMARY KEY ( readerId , isbn )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;


CREATE TABLE Search(
	SearchID int,
	Criteria int,
	Type int
);

CREATE TABLE SaveSearchs(
	UserID int,
	SearchID int,
	Date datetime
);
